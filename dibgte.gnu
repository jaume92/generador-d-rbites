# Definicions per dibuixar ground tracks
# Adaptat per òrbites Molniya (mnpr02)
#
# CAL CARREGAR: ctants.gnu
#
# CAL DEFINIR:
# - ipert : 1 per tenir en compte la pert. del J2, 0 per no tenir-la en compte
# - lonesp : longitud del meridià del lloc a espiar
# - xc : excentricitat de l'òrbita
# - T : període de l'òrbita
# - i : inclinació de l'òrbita
# - tf : durada de la simulació (segons)
# - nt : nombre de punts de la simulació

# Equació de l'òrbita
r(nu)=a*(1-xc**2)/(1+xc*cos(nu))

# Terme J2 del potencial gravitatori terrestre
J2=ipert*0.0010826267

# Moviment mig
n=2*pi/T

# Semieix a partir de la 3a llei de Kepler
a=(mutr/n**2)**(1./3)

# Paràmetre a partir de semieix i excentricitat
p=a*(1-xc**2)

# Retrogradació de l'argument del node asc per segon (Vallado 3rd ed p. 645)
DOmg=-3*n*(Rtr**2)*J2/(2*(p**2))*cos(i)

# Retrogradació de l'argument del peri per segon (Vallado 3rd ed p. 647)
Domg=3*n*(Rtr**2)*J2/(4*(p**2))*(4-5*sin(i)**2)

# Argument del "node asc" com a funció del temps
Omg(t)=DOmg*t

# Argument del peri com a funció del temps
omg(t)=-pi/2+Domg*t

# Posició a sobre de l'òrbita
xnu(nu)=r(nu)*cos(nu)
ynu(nu)=r(nu)*sin(nu)
znu(nu)=0

# Rotació al pla orbital donada per argument del peri
xomg(nu,t)=cos(omg(t))*xnu(nu)-sin(omg(t))*ynu(nu)
yomg(nu,t)=sin(omg(t))*xnu(nu)+cos(omg(t))*ynu(nu)
zomg(nu,t)=znu(nu)

# Rotació del pla orbital donada per la inclinació
xi(nu,t)=xomg(nu,t)
yi(nu,t)=cos(i)*yomg(nu,t)-sin(i)*zomg(nu,t)
zi(nu,t)=sin(i)*yomg(nu,t)+cos(i)*zomg(nu,t)

# Rotació del pla orbital donada per l'argument del node
xOmg(nu,t)=cos(Omg(t))*xi(nu,t)-sin(Omg(t))*yi(nu,t)
yOmg(nu,t)=sin(Omg(t))*xi(nu,t)+cos(Omg(t))*yi(nu,t)
zOmg(nu,t)=zi(nu,t)

# "Ascensió recta" del satèl·lit (xyz amb y al meridià del lloc a espiar
# a temps zero)
alf(nu,t)=atan2(yOmg(nu,t),xOmg(nu,t))

# Declinació del satèl·lit
dlt(nu,t)=asin(zOmg(nu,t)/r(nu))

# "Temps sideri" (xyz com abans) del lloc a espiar
the(t)=pi/2+omgtr*t

# Determinació principal d'un angle (a [-pi:pi])
detpr(th)=th-floor((th+pi)/(2*pi))*(2*pi)

# Latitud i longitud en graus
latd(t,nu)=dlt(nu,t)/pi*180
lond(t,nu)=detpr( lonesp+alf(nu,t)-the(t) )/pi*180

# Paràmetres de dibuix
set xrange [-180:180]
set yrange [-90:90]
set size ratio -1
unset key

# Comanda per generar t M E
cmdpl=sprintf( \
   "./kplt2nu %.16G=e %.16G=T %.16G=M0 %.16G=tf %d=nt > gtrk.txt",\
   xc,T,pi,tf,nt)
set zeroaxis

system cmdpl

plot 'wm.txt' w l,'gtrk.txt' u (lond($1,$3)):(latd($1,$3)) w p lt 3 pt 2

