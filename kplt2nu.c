#include <stdio.h>
#include <math.h>
#include "bisnwt.h"

double e, M;

double funcio(double x, void *prm)
{return (x-e*sin(x)-M);}

double dfuncio(double x, void *prm)
{return (1-e*cos(x));}


int main (int argu, char *arg[])
{
   int  nt, i,  maxit=10, q;
   double T, M0, tf, tp, v, t, E, dlt=6.5, tol=1E-12;
  
   if (argu!=6
       || sscanf(arg[1], "%lf", &e)!=1
       || sscanf(arg[2], "%lf", &T)!=1
       || sscanf(arg[3], "%lf", &M0)!=1
       || sscanf(arg[4], "%lf", &tf)!=1
       || sscanf(arg[5], "%d", &nt)!=1
     )
   {
      fprintf(stderr,"ERROR: S'ha realitzat una entrada de dades defectuosa\n");
      return 0;
   }
   
   tp=-T*M0/(2*M_PI);
   
   for (i=0; i<=nt; i++)
   {
      t=i*(tf/nt);
      M=(2*M_PI*(t-tp))/T;
      bisnwt(M-M_PI, M+M_PI, &E, &dlt, tol, maxit, funcio, dfuncio, NULL);
      v=acos((e-cos(E))/(e*cos(E)-1));
      q=floor(E/M_PI);
     
      if ((q & 0x01)==1) 
      {
        v=-v+(q+1)*M_PI;
      }
     
      else
      {
        v=q*M_PI+v;
      }
     
      printf("%.16G	%.16G	%.16G\n",t,M,v);
   }

   return 0;
}

