#include <stdio.h>
#include <math.h>
#include "bisnwt.h"

int bisnwt(double a, double b, double *arr, double *dlt, double tol, int maxit, double (*f)(double,void*), double (*df)(double, void*), void *prm)
{
  double c, y, fc;
  int i;
  
  while(1)
  {
    
    while(fabs(b-a)>*dlt)
    { 
      c=(a+b)/2;
      fc=(*f)(c,prm);
      
      if(fc<0)
      {
	a=c;
      }
      
      else
      {
	b=c;
      }
    }
    
    if(*dlt<=tol)
    {
      *arr=c;
      return -1;
    }
    
    else
    {
      
      for (i=0; i<maxit; i++)
      {
	y=c-f(c,prm)/(df(c,prm));
	
	if(fabs(y-c)<tol)
	{
	  *arr=c;
	  return i+1;
	}
	
	c=y;
      }
    }
    
  *dlt=*dlt/2;
  }
}