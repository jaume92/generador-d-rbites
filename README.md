# Generador d'òrbites #

Es tracta d'un problema que vaig fer per l'assignatura de mètodes numèrics l'any 2013.

S'utilitzen les lleis de Kepler per generar òrbites a partir d'uns certs paràmetres.

### Per executar ###

**1.** Escrivim ``make kplt2nu``al terminal per compilar els arxius.

**2.** Obrim *gnuplot* i escrivim:
```
> load ’ctants.gnu’
> lonesp=0   # longitud 
> xc=0.74105 # excentricitat
> T=dias/2   # període de l’òrbita
> i=63.4*(pi/180) # inclinació de l’òrbita (en radians)
> tf=dias # temps
> nt=333 # número de punts
> load ’dibgte.gnu’ (aquí executem *kplt2nu* i calculem les òrbites)
```
![Captura de pantalla 2016-11-05 a les 16.57.24.png](https://bitbucket.org/repo/bEGdGG/images/154341645-Captura%20de%20pantalla%202016-11-05%20a%20les%2016.57.24.png)

### Altres exemples ###

Utilitzant *lonesp=0.38  xc=0.74105  T=dias/3  i=63.4(pi/180)  tf=dias  nt=1000*

![Captura de pantalla 2016-11-05 a les 16.59.57.png](https://bitbucket.org/repo/bEGdGG/images/1014162910-Captura%20de%20pantalla%202016-11-05%20a%20les%2016.59.57.png)